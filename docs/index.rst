Welcome to Crunch/Inotify's documentation!
==========================================

`API-documentation. <_static/index.html>`_

Contents:

.. toctree::
   :maxdepth: 2

   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

