<?php
namespace Crunch\Inotify;

/**
 * Symbolic interface
 *
 * It's not required for a receiver to implement this interface, any callable is accepted. This interfacce
 * describes the expected signature.
 */
interface Receiver {
    /**
     * Gets invoke on every event
     *
     * @param Event   $event
     * @param Handler $handler
     * @return mixed
     */
    public function __invoke (Event $event, Handler $handler);
}
