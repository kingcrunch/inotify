<?php
namespace Crunch\Examples;

use Crunch\Inotify\Handler;
use Crunch\Inotify\Event;

require __DIR__ . '/../vendor/autoload.php';


$handler = new Handler;
$handler->register(__DIR__, Event::ACCESS, function (Event $event, Handler $handler) {
    var_dump($event->name);
});
#$handler->wait(Handler::INFINITE_WAIT);

$handler->cyclicWait(3, function (Handler $handler, $count) {
    echo date('c') . ": $count Events". \PHP_EOL;
});

// Now call something like
// $ cat default.php
// in a different console
