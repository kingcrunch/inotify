<?php
namespace Crunch\Inotify;

class Event {
    const ACCESS = \IN_ACCESS;
    const MODIFY = \IN_MODIFY;
    const ATTRIBUTE = \IN_ATTRIB;
    const CLOSE_WRITE = \IN_CLOSE_WRITE;
    const CLOSE_NOWRITE = \IN_CLOSE_NOWRITE;
    const OPEN = \IN_OPEN;
    const MOVED_TO = \IN_MOVED_TO;
    const MOVED_FROM = \IN_MOVED_FROM;
    const CREATE = \IN_CREATE;
    const DELETE = \IN_DELETE;
    const DELETE_SELF = \IN_DELETE_SELF;
    const MOVE_SELF = \IN_MOVE_SELF;
    const CLOSE = \IN_CLOSE;
    const MOVE = \IN_MOVE;
    const ALL = \IN_ALL_EVENTS;
    const UNMOUNT = \IN_UNMOUNT;
    const QUEUE_OVERFLOW = \IN_Q_OVERFLOW;
    const IGNORED = \IN_IGNORED;
    const ISDIR = \IN_ISDIR;
    const ONLYDIR = \IN_ONLYDIR;
    const DONT_FOLLOW = \IN_DONT_FOLLOW;
    const MASK_ADD = \IN_MASK_ADD;
    const ONESHOT = \IN_ONESHOT;

    /**
     * ID of the registered receiver
     *
     * @var int
     */
    public $descriptor;

    /**
     * Bitmask of events, from which at least one triggered this event
     *
     * @var int
     */
    public $mask;

    /**
     * A session ID, that helps identifying related events
     *
     * Two events may be related to each other, like MOVE_FROM and MOVE_TO.
     *
     * @var int
     */
    public $session;

    /**
     * Pathname of the resource, that triggered this event
     *
     * @var string
     */
    public $name;

    public function __construct ($descriptor, $mask, $cookie, $name) {
        $this->descriptor = $descriptor;
        $this->mask = $mask;
        $this->session = $cookie;
        $this->name = $name;
    }
}
