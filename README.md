Crunch\Inotify [![Build Status](https://secure.travis-ci.org/KingCrunch/Inotify.png)](http://travis-ci.org/KingCrunch/Inotify)
============
Inotify library

* [Documentation at readthedocs.org](http://crunchinotify.readthedocs.org/en/latest/)
* [List of available packages at packagist.org](http://packagist.org/packages/crunch/inotify)

Requirements
============
* PHP => 5.3
* [pecl/inotify](http://php.net/book.inotify), [inotify at PECL](http://pecl.php.net/package/inotify)

Contributors
============
See CONTRIBUTING.md for details on how to contribute.

* Sebastian "KingCrunch" Krebs <krebs.seb@gmail.com> -- http://www.kingcrunch.de/ (german)

License
=======
This library is licensed under the MIT License. See the LICENSE file for details.
