<?php
namespace Crunch\Inotify;

class Handler {
    const NO_WAIT = 0;
    const INFINITE_WAIT = null;

    protected $_res;

    /**
     * @var Receiver[]
     */
    protected $_receiver = array();

    public function __construct () {
        $this->_res = \inotify_init();
        stream_set_blocking($this->_res, 0);
    }

    /**
     * Cleanup
     *
     * Will also fetch every remaining event
     */
    public function __desctruct () {
        $this->wait(self::NO_WAIT);
        foreach (array_keys($this->_receiver) as $id) {
            \inotify_rm_watch ($id);
            unset($this->_receiver[$id]);
        }
    }

    /**
     * Register new event receiver
     *
     * @param string $path
     * @param int $events
     * @param callable $receiver
     * @return int
     */
    public function register ($path, $events, $receiver) {
        $id = \inotify_add_watch($this->_res, $path, $events);
        $this->_receiver[$id] = $receiver;
        return $id;
    }

    /**
     * Waits at most `$timeout` seconds to receive inotify events and trigger registered receivers
     *
     * @param int|null $timeout
     * @return int
     */
    public function wait ($timeout = null) {
        $read = array($this->_res);
        $write = $except = array();
        $count = 0;
        if (stream_select($read, $write, $except, $timeout)) {
            foreach (\inotify_read($this->_res) as $event) {
                $receiver = $this->_receiver[$event['wd']];
                /** @var callable $receiver */
                $receiver(new Event($event['wd'], $event['mask'], $event['session'], $event['name']), $this);
                $count++;
            }
        }
        return $count;
    }

    /**
     * Cyclic calls wait() for at most $timeout seconds and repeat
     *
     * $intermediate gets call between every call of wait(). It can interrupt the process by returning true.
     *
     * @param int $timeout
     * @param callable $intermediate
     */
    public function cyclicWait ($timeout, $intermediate) {
        do {
            $count = $this->wait($timeout);
        } while (!$intermediate($this, $count));
    }
}
